#!/usr/bin/env python
# -*- coding: utf-8 -*-
import telebot, db
from botmain import bot
from collections import deque
import pmrsu
from telebot import types

u_list = deque()

def fill_queue():
    u_list.clear()
    try:
        with db.connection.cursor() as c:
            sql = "SELECT tgid,login,pass FROM users WHERE ch_flag = 1"
            c.execute(sql)
            list_ = c.fetchall()
            for el in list_:
                u_list.append(el)
            if not u_list:
                return 0
            return 1
    except:
        return 0

def check_old(tgid):
    try:
        with db.connection.cursor() as c:
            sql = "SELECT nt_uspev,nt_msg FROM users WHERE tgid = {0}".format(tgid)
            c.execute(sql)
            list_ = c.fetchone()
            nums = [list_['nt_uspev'],list_['nt_msg']]
            return nums
    except:
        return 0

def set_new_values(tgid,uspev,msg):
    try:
        with db.connection.cursor() as c:
            sql = f"UPDATE `users` SET `nt_uspev` = '{uspev}', `nt_msg` = '{msg}' WHERE `tgid` = '{tgid}'"
            c.execute(sql)
            db.connection.commit()
            return 1
    except:
        return 0
    
def go_notifies():
    q = fill_queue()
    if q == 0:
        return 0   
    while True:
        try:
            s_data = u_list.popleft()
            student = pmrsu.StudentProfile(s_data['login'],s_data['pass'])
            if(student.get_lstate() == 0):
                continue
            uspev = list(student.getRatingNotifies().values()) # Новые баллы
            msg = list(student.getCommunicationNotifies().values()) # Новые сообщения
            count = [0,0] 
            for i in uspev:
                count[0] += int(i)
            for i in msg:             
                count[1] += int(i)  
            if count == check_old(s_data['tgid']) or count == [0,0]:
                set_new_values(s_data['tgid'],count[0],count[1])
                continue
            else:
                keyboard = types.InlineKeyboardMarkup()
                k_add = 0
                if(count[0] > 0):
                    key_uspev = types.InlineKeyboardButton(text="Посмотреть баллы", url="https://p.mrsu.ru/Learning/Student/Subjects")
                    keyboard.add(key_uspev)
                    k_add += 1
                if(count[1] > 0):
                    key_msg = types.InlineKeyboardButton(text="Посмотреть сообщения", url="https://p.mrsu.ru/Learning/Student/Subjects?t=m")
                    keyboard.add(key_msg)
                    k_add += 1
                response = '<b>Хэй, кажется у тебя есть что-то новое: </b>\nНовые баллы: {0} \nНовые сообщения: {1}\nПодробнее: /check'.format(count[0],count[1])
            set_new_values(s_data['tgid'],count[0],count[1])
            if k_add == 0:
                bot.send_message(s_data['tgid'], response, parse_mode="HTML")
            else:
                bot.send_message(s_data['tgid'], response, parse_mode="HTML",reply_markup=keyboard)
        except IndexError:
            break

if __name__ == "__main__":
    go_notifies()

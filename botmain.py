#!/usr/bin/env python
# -*- coding: utf-8 -*-
import telebot #библиотека для работы с ботами
import datetime #работа с датой/временем для лога
import pmrsu #работа с ЭИОС
import os
from tg_user import User
from telebot import types

TOKEN = '1336035155:AAH5nJa-oDBBbQzVFpF8po3t8tXu15Ope3s'

def clog(text): #лог в консоль с датой и временем
    time = datetime.datetime.today().strftime("[%d-%m-%Y][%H:%M:%S]")
    print(time + " " + text)

bot = telebot.TeleBot(TOKEN);

@bot.message_handler(commands=['start'])
def start_bot(message):
    u = User(message.from_user.id)
    u.setsost(0)
    bot.send_message(u.getid(), "Привет, это #ботЭИОС :)\n\nЧтобы получить доступ к функциям бота, необходимо ввести логин и пароль от личного кабинета (p.mrsu.ru)")
    bot.send_message(u.getid(), "Введи логин:")

@bot.message_handler(commands=['setcheck'])
def set_check_flag(message):
    u = User(message.from_user.id)
    sost = u.getsost()
    if sost != 2:
        return 0
    flag = u.getcheckflag()
    if flag:
        u.setcheckflag(0)
        bot.send_message(u.getid(), 'Вам больше <b>не будут</b> приходить уведомления о новых баллах и сообщениях\n'
                         'Чтобы снова включить уведомления, воспользуйтесь командой /setcheck', parse_mode="HTML")
    else:
        u.setcheckflag(1)
        bot.send_message(u.getid(), 'Теперь Вам <b>будут приходить</b> уведомления о новых баллах и сообщениях\n'
                         'Чтобы отключить уведомления, введите команду /setcheck еще раз :)', parse_mode="HTML")
        
    
@bot.message_handler(commands=['help'])
def helpmsg(message):
    u = User(message.from_user.id)
    sost = u.getsost()
    if sost != 2:
        return 0
    bot.send_chat_action(message.chat.id,'typing')
    bot.send_message(u.getid(), '<b>Команды #ботЭИОС</b>:\n\n'
                     '<i>Общее:</i>\n'
                     '<b>/start</b> - сброс настроек\n'
                     '<b>/delete</b> - удалить все свои данные, закончить работу с ботом\n\n'
                     '<i>Функции ЭИОС:</i>\n'
                     '<b>/check</b> - проверить новые события (Успеваемость и Общение)\n'
                     '<b>/ttable</b> - расписание на сегодня\n'
                     '<b>/ttableweek</b> - расписание по дням недели\n'
                     '<b>/message</b> - отправить сообщение в "Общение"\n'
                     '\n<i>Специальные возможности:</i>\n'
                     '<b>/talanov</b> - отметиться у Таланова (только 341 группа ИЭС)', parse_mode="HTML")
        
@bot.message_handler(commands=['check'])
def check(message):
    u = User(message.from_user.id)
    sost = u.getsost()
    if sost != 2:
        return 0
    bot.send_chat_action(message.chat.id,'typing')
    student = pmrsu.StudentProfile(u.getlogin().encode().decode('utf-8'),u.getpass())
    uspev = student.getRatingNotifies()
    msg = student.getCommunicationNotifies()
    ans_str = '<b>Новые события: </b>\n\n'
    if not uspev:
        ans_str += 'Успеваемость: <i> не найдено </i>\n'
    else:
        ans_str += 'Успеваемость - Новые баллы: \n'
        for i in uspev:
            ans_str += '<b>{0}</b> - {1} \n'.format(i,uspev[i])
    if not msg:
        ans_str += '\nОбщение: <i> не найдено </i> \n'
    else:
        ans_str += '\nОбщение - Новые сообщения: : \n'
        for i in msg:
            ans_str += '<b>{0}</b> - {1} \n'.format(i,msg[i])
    bot.send_message(u.getid(), ans_str, parse_mode="HTML")
    
@bot.message_handler(commands=['ttable'])
def timetable(message):
    u = User(message.from_user.id)
    sost = u.getsost()
    if sost == 2:
        bot.send_chat_action(message.chat.id,'typing')
        student = pmrsu.StudentProfile(u.getlogin().encode().decode('utf-8'),u.getpass())
        ttable = student.getTimeTableFull()
        if(not ttable):
            response = '<b>Пар сегодня нет :)</b>'
        else:
            response = '<b>Расписание на сегодня:</b> \n\n'
            for i in ttable:
                response += '{0} \n\n'.format(i)
        bot.send_message(u.getid(), response, parse_mode="HTML")

@bot.message_handler(commands=['talanov'])
def talanov(message):
    u = User(message.from_user.id)
    sost = u.getsost()
    if sost < 3:
        bot.send_chat_action(message.chat.id,'typing')
        student = pmrsu.StudentProfile(u.getlogin().encode().decode('utf-8'),u.getpass())
        t = student.TalanovCheck()
        if t == 1:
            bot.send_message(u.getid(), 'Вы успешно отметились у Виктора Михайловича Таланова!')
        elif t == -1:
            bot.send_message(u.getid(), 'Вам недоступна эта функция :/')
        else:
            bot.send_message(u.getid(), 'Отметиться не получилось, придется вручную :(')

@bot.message_handler(commands=['ttableweek'])
def ttableweek(message):
    u = User(message.from_user.id)
    sost = u.getsost()
    if(sost != 2):
        return 0
    student = pmrsu.StudentProfile(u.getlogin().encode().decode('utf-8'), u.getpass())
    bot.send_chat_action(message.chat.id,'typing')
    subjects = student.getDaysFromTimeTable()
    keys_list = list(subjects)
    keyboard = types.InlineKeyboardMarkup()
    for k in keys_list:
        key = types.InlineKeyboardButton(text=str(k),callback_data='{0}*{1}'.format(str(k),str(subjects[k])))
        keyboard.add(key)
    u.setsost(4)
    key_exit = types.InlineKeyboardButton(text='❌ ОТМЕНИТЬ',callback_data='-1')
    keyboard.add(key_exit)
    bot.send_message(u.getid(), text='Выбери день недели:', reply_markup=keyboard)
    
@bot.message_handler(commands=['message','teacher'])
def msg_and_teacher(message):
    u = User(message.from_user.id)
    sost = u.getsost()
    if(sost == 2):
        student = pmrsu.StudentProfile(u.getlogin().encode().decode('utf-8'), u.getpass())
        subjects = list(student.getSubjectsWithLink())
        keyboard = types.InlineKeyboardMarkup()
        for s in range(0, len(subjects)):
            key = types.InlineKeyboardButton(text=str(subjects[s]),callback_data=str(s))
            keyboard.add(key)
        key_exit = types.InlineKeyboardButton(text='❌ ОТМЕНИТЬ',callback_data='-1')
        keyboard.add(key_exit)
        bot.send_message(u.getid(), text='Выбери дисциплину:', reply_markup=keyboard)
        if message.text == '/teacher':
            u.setsost(5)
        else:
            u.setsost(3)


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    if call.message:
        u = User(call.from_user.id)
        if(call.data == "-1" and u.getsost() > 2):
            bot.answer_callback_query(callback_query_id=call.id,show_alert=False,text="Отменено!")
            bot.send_message(u.getid(),text='Ваше действие отменено😉')
            u.setsost(2)
            bot.delete_message(call.message.chat.id,call.message.message_id)
            return 0
        if(u.getsost() < 2 and u.getsost() > 4):
            return 0
        s = pmrsu.StudentProfile(u.getlogin().encode().decode('utf-8'), u.getpass())
        if(u.getsost() == 3 or u.getsost() == 5):
            sbj = s.getSubjectsWithLink()
            sblist_val = list(sbj.values())
            sblist_keys = list(sbj.keys()) # ссылки
            selected = sblist_val[int(call.data)]
            
            id_ = selected.split('=')[1]
            if(id_.find('#') != -1):
                id_ = id_.split('#')[0]
            
            if(u.getsost() == 5):
                response = "<b>Дисциплина</b>: {0} \n<b>Преподаватель</b>: ".format(sblist_keys[int(call.data)])
                t_list = s.getTeacherName(id_)
                for ts in t_list:
                    response += ts.string + ' \n'
                bot.answer_callback_query(callback_query_id=call.id,show_alert=False,
                                          text="Секундочку...")
                bot.send_message(u.getid(),text=response, 
                                 parse_mode="HTML")
                bot.delete_message(call.message.chat.id,call.message.message_id)
                u.setsost(2)
                return 1
            
            bot.answer_callback_query(callback_query_id=call.id,show_alert=False,text='Выбрана дисциплина: ' + sblist_keys[int(call.data)])
            keyboard = types.InlineKeyboardMarkup()
            key = types.InlineKeyboardButton(text="Отменить отправку сообщения",callback_data="-1")
            keyboard.add(key)
            bot.send_message(u.getid(),text='Выбран предмет: <b>' + sblist_keys[int(call.data)] + '</b>\n\nВведи текст, который необходимо отправить в раздел "Общение": ', parse_mode="HTML", reply_markup=keyboard)
            u.msg_dset(id_)
            u.setsost(3)
            bot.delete_message(call.message.chat.id,call.message.message_id)
            return 1
        elif(u.getsost() == 4):
            bot.answer_callback_query(callback_query_id=call.id,show_alert=False,text='Ищем расписание...')
            text = call.data.split('*')[0]
            tick = call.data.split('*')[1]
            ttable = s.getTimeTableFull(tick)
            if(not ttable):
                response = '<b>Пар нет :)</b>'
            else:
                response = '<b>Расписание ({0}):</b> \n\n'.format(text)
                for i in ttable:
                    response += '{0} \n\n'.format(i)
                bot.send_message(u.getid(), response, parse_mode="HTML")
            u.setsost(2)
            bot.delete_message(call.message.chat.id,call.message.message_id)
            return 1
            
@bot.message_handler(commands=['delete'])
def delete_data(message):
    u = User(message.from_user.id)
    sost = u.getsost()
    if sost > 0 and sost < 3:
        if u.delete() == 1:
            bot.send_message(message.from_user.id, "Ваши данные для этого бота удалены!\nЧтобы начать пользоваться ботом заново, введите /start")
        else:
            pass

@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    u = User(message.from_user.id)
    sost = u.getsost()
    if sost == 0:
        if(u.checkreg() == 0):
            from tg_user import new_user
            new_user(message.from_user.id)        
        u.setlogin(message.text)
        u.setsost(1)
        bot.send_message(u.getid(), "Отлично, логин записан! Настала очередь ввести пароль:")
    elif sost == 1:
        u.setpass(message.text)
        u.setsost(2)
        bot.send_message(u.getid(), "Пароль принят! Попробуем войти в систему...")
        student = pmrsu.StudentProfile(u.getlogin().encode().decode('utf-8'),u.getpass())
        if student.get_lstate() == 1:
            bot.send_message(u.getid(), "Авторизация в личном кабинете прошла успешно!")
            response = '<b>Информация о пользователе:</b>\n\n<b>Студент</b>: {0}\n<b>Факультет</b>:{1}\n<b>Группа</b>:{2}'.format(student.getFIO(),student.getFacult(),student.getGroup())
            bot.send_message(u.getid(), response, parse_mode="HTML")
        else:
            bot.send_message(u.getid(), "Логин:" + u.getlogin() + "\nПароль: " + u.getpass())
            bot.send_message(u.getid(), "Что-то пошло не так... \nДавай по-новой! Вводи логин:")
            u.setsost(0)
    elif sost == 3:
        student = pmrsu.StudentProfile(u.getlogin().encode().decode('utf-8'),u.getpass())
        d_id = u.msg_dget()
        if student.sendMessage(d_id,message.text) == 1:
            bot.send_message(u.getid(), "Сообщение успешно отправлено!")
            u.setsost(2)
        else:
            bot.send_message(u.getid(), "Отправить сообщение не удалось :(")
            u.setsost(2)
        
if __name__ == "__main__":
    clog("Бот запущен")
    bot.polling()
        
#СОСТОЯНИЯ ПОЛЬЗОВАТЕЛЕЙ:

#0 - чисто старт (данных нет)
#1 - введен логин
#2 - введен логин, введен пароль

#3 - общение (ввод текста письма)
#4 - расписание
#5 - меню кто преподаватель
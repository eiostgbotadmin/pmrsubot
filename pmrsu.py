#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup

URL = "https://p.mrsu.ru/Account/Login?ReturnUrl=%2FPortfolio%2FMain"
MSGURL = "https://p.mrsu.ru/Learning/Student/"

def check_logged(method):
    def checklogstudent(self, *args, **kwargs):
        if(self.logged == 0):
            raise Exception("Student hasn't logged")
        else:
            method(self, *args, **kwargs)
    return checklogstudent

class StudentProfile():
    
    def __init__(self, login, password):
        self.lgn = login
        self.pwd = password
        self.__login()
        
    def __initialize_asp(self, page_var):
        self.soup=BeautifulSoup(page_var.content, features='html.parser')
        self.VIEWSTATE=self.soup.find(id="__VIEWSTATE")['value']
        self.VIEWSTATEGENERATOR=self.soup.find(id="__VIEWSTATEGENERATOR")['value']
        self.EVENTVALIDATION=self.soup.find(id="__EVENTVALIDATION")['value'] 
        
    def __login(self): #вход в систему
        self.session = requests.Session()
        #получение служебных полей
        self.r=self.session.get(URL)
        self.__initialize_asp(self.r)
        #сборка формы
        login_data={"__VIEWSTATE":self.VIEWSTATE,
        "__VIEWSTATEGENERATOR":self.VIEWSTATEGENERATOR,
        "__EVENTVALIDATION":self.EVENTVALIDATION,
        "ctl00$MainContent$UserName":self.lgn,
        "ctl00$MainContent$Password":self.pwd,
        "ctl00$MainContent$Btn_SignIn":"Вход"}
        #запрос
        self.r=self.session.post(URL, data=login_data)
        if self.r.url == 'https://p.mrsu.ru/Portfolio/Main':
            self.logged = 1
        else:
            self.logged = 0
            
    def get_lstate(self):
        return self.logged
    
    def getFIO(self): #получение ФИО из эиос
        self.r = self.session.post("https://p.mrsu.ru/Portfolio/Main")
        self.soup=BeautifulSoup(self.r.content, features='html.parser')
        x = self.soup.select('td > span')
        return x[1].text
            
    #@check_logged
    def getTimeTableFull(self, tick='-1'):
        url = "https://p.mrsu.ru/Learning/TimeTable/TimeTable"
        if tick != '-1':
            url += '?tick='+tick
        self.r = self.session.post(url)
        self.soup = BeautifulSoup(self.r.content, features='html.parser')
        tab = self.soup.find_all('tr')[38:]
        self.soup = BeautifulSoup(str(tab), features='html.parser')
        td = self.soup.find_all('td')
        rasp = []
        rasp_str = ''
        num = ''
        check_group = 1
        for el in td:
            para = 0
            if('tm-first-child' in el.get('class')):
                num = el.text
                continue
            else:
                self.soup = BeautifulSoup(str(el), features='html.parser')
                para = 0 if len(self.soup.select('a')) == 0 else 1
                rasp_str += num
                if(el.get('style') == 'width:47%;' and check_group == 1):
                    if(para == 1):
                        rasp_str += '[1 ПОДГРУППА] '
                    check_group = 2
                elif(el.get('style') == 'width:47%;' and check_group == 2):
                    if(para == 1):
                        rasp_str += '[2 ПОДГРУППА] '
                    check_group = 1
                if(len(self.soup.select('a')) > 0):
                    rasp_str = rasp_str + self.soup.select('a')[0]['title'] + ' '
                    if(len(self.soup.select('a')) > 2):
                        rasp_str = rasp_str + '\n(' + self.soup.select('a')[1]['title'] + ') \n' + self.soup.select('a')[2]['title']
                    elif(len(self.soup.select('a')) > 1):
                        rasp_str = rasp_str + '[ДИСТАНЦИОННО] \n' + self.soup.select('a')[1]['title']
                    else:
                        continue
            if(len(rasp_str) > 2):
                rasp.append(rasp_str)
            rasp_str = ''
        return rasp
    
    def getFacult(self):
        self.r = self.session.post("https://p.mrsu.ru/Learning/TimeTable/TimeTable")
        self.soup = BeautifulSoup(self.r.content, features='html.parser')
        return self.soup.select('h3')[1].text.split('группа')[0][:-1]
    
    def getGroup(self):
        self.r = self.session.post("https://p.mrsu.ru/Learning/TimeTable/TimeTable")
        self.soup = BeautifulSoup(self.r.content, features='html.parser')
        return str(self.soup.select('h3 > small')[0].text.split(':')[1][1:])

    def TalanovCheck(self):
        if self.getGroup() != "341-1" and self.getGroup() != "341-2":
            return -1
        else:
            self.r=self.session.get("https://p.mrsu.ru/Learning/Student/m?id=1235938")
            self.__initialize_asp(self.r)
            self.data={"__VIEWSTATE":self.VIEWSTATE,
            "__VIEWSTATEGENERATOR":self.VIEWSTATEGENERATOR,
            "__EVENTVALIDATION":self.EVENTVALIDATION,
            "ctl00$MainContent$ctl01$ListView1$ctrl30$MessageTextBox":"Я",
            "ctl00$MainContent$ctl01$ListView1$ctrl30$InsertButton":"Отправить"}
            try:
                self.r=self.session.post("https://p.mrsu.ru/Learning/Student/m?id=1235938", data=self.data)
                return 1
            except:
                return 0

    def getSubjectsWithLink(self):
        self.r = self.session.post("https://p.mrsu.ru/Learning/Student/Subjects?t=m")
        self.soup = BeautifulSoup(self.r.content, features='html.parser')
        blok = self.soup.find("ul", class_="nav nav-pills nav-stacked").find_all('a')
        predmets = {}
        for i in blok:
            string = str(i.text).replace("\r\n                                ","").replace("\r\n                            ","")
            try:
                string = string.split(str(i.find('span').string))[0]
            except AttributeError:
                pass
            if(string == "None" or not string):
                continue
            predmets[string] = i.get('href')
        return predmets
    
    def sendMessage(self, d_id, text):
        url_msg = "https://p.mrsu.ru/Learning/Student/m?id={0}".format(str(d_id))
        self.r=self.session.get(url_msg)
        self.__initialize_asp(self.r)
        self.data={"__VIEWSTATE":self.VIEWSTATE,
        "__VIEWSTATEGENERATOR":self.VIEWSTATEGENERATOR,
        "__EVENTVALIDATION":self.EVENTVALIDATION,
        "ctl00$MainContent$ctl01$ListView1$ctrl30$MessageTextBox": text,
        "ctl00$MainContent$ctl01$ListView1$ctrl30$InsertButton":"Отправить"}
        try:
            self.r=self.session.post(url_msg, data=self.data)
            return 1
        except:
            return 0
    
    def getRatingNotifies(self):
        self.r = self.session.post("https://p.mrsu.ru/Learning/Student/Subjects")
        self.soup = BeautifulSoup(self.r.content, features='html.parser')
        g = self.soup.select("ul>li>a>span")
        rating_list = {}
        for i in range(0,len(g)):
            if(g[i].get('title') == "Количество непросмотренных оценок"):
                rating_notifs = g[i].text
                rating_name = ' '.join(g[i].find_parent().text.split()).replace(rating_notifs,'')
                rating_list[rating_name] = rating_notifs
            else:
                continue
        return rating_list
    
    def getCommunicationNotifies(self):
        self.r = self.session.post("https://p.mrsu.ru/Learning/Student/Subjects?t=m")
        self.soup = BeautifulSoup(self.r.content, features='html.parser')
        g = self.soup.select("ul>li>a>span")
        rating_list = {}
        for i in range(0,len(g)):
            if(g[i].get('title') == "Количество непрочитанных сообщений"):
                rating_notifs = g[i].text
                rating_name = ' '.join(g[i].find_parent().text.split()).replace(rating_notifs,'')
                rating_list[rating_name] = rating_notifs
            else:
                continue
        return rating_list
    
    def getDaysFromTimeTable(self):
        self.r = self.session.post("https://p.mrsu.ru/Learning/TimeTable/TimeTable")
        self.soup = BeautifulSoup(self.r.content, features='html.parser')
        ul = self.soup.find("ul",class_="pagination")
        self.soup = BeautifulSoup(str(ul), features='html.parser')
        timetable = {}
        pr = self.soup.select("li > a")[1:7]
        for ds in pr:
            timetable[ds.text] = ds['href'].split('=')[1]
        return timetable
    
    def getTeacherName(self,d_id):
        self.r = self.session.post("https://p.mrsu.ru/Learning/Student/Rating?id="+str(d_id))
        self.soup = BeautifulSoup(self.r.content, features='html.parser')
        t_list = []
        teacher = self.soup.select("td > strong")
        for t in teacher:
            t_list.append(t)
        return t_list
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import db

def new_user(tgid):
    try:
        with db.connection.cursor() as c:
            query = "INSERT INTO `users` (tgid,login,pass,sost,smsg,nt_uspev,nt_msg) VALUES ('{0}','','','0','0','0','0')".format(tgid)
            c.execute(query)
            db.connection.commit()
            return 1
    except:
        return 0

class User(): #пользователь
    
    def __init__(self,uid): #инициализация
        self.id = uid
        self.login = ''
        self.pwd = ''
        self.sost = 0
        self.smsg = 0
        self.checkflag = 0
        if(self.checkreg() != 0):
            self.__getdata() #получение данных из файла настроек
    
    def checkreg(self):
        try:
            with db.connection.cursor() as c:
                sql = "SELECT `id` FROM `users` WHERE `tgid` = {0}".format(self.id)
                c.execute(sql)
                result = c.fetchone()
                if result == None:
                    return 0
                else:
                    return 1
        except Exception as e:
            print(e)
        
    def __getdata(self): #получение данных из файла настроек
        try:
            with db.connection.cursor() as c:
                sql = "SELECT * FROM `users` WHERE `tgid` = '%s'"
                c.execute(sql,(self.id))
                result = c.fetchone()
                if result == None:
                    return 0
                self.login = result['login']
                self.pwd = result['pass']
                self.sost = result['sost']
                self.smsg = result['smsg']
                self.checkflag = result['ch_flag']
        except Exception as e:
            print(e)
    
    def setlogin(self, login): 
        self.login = login
        self.__savedata()
    def setpass(self, pwd):
        self.pwd = pwd
        self.__savedata()
    
    def msg_dset(self,smsg):
        self.smsg = smsg
        self.__savedata()
    def msg_dget(self):
        return self.smsg
        
    def getlogin(self): 
        return self.login
    def getpass(self):
        return self.pwd
        
    def getid(self):
        return self.id
    
    def setsost(self,sost):
        self.sost = sost;
        self.__savedata()
        
    def getsost(self):
        return self.sost
    
    def getcheckflag(self):
        return self.checkflag
    def setcheckflag(self,flag):
        self.checkflag = flag
        self.__savedata()
    
    def __savedata(self): #сохранение настроек
        try:
            with db.connection.cursor() as c:
                sql = f"UPDATE `users` SET `login` = '{self.login}', `pass` = '{self.pwd}', `sost` = '{self.sost}', `smsg` = '{self.smsg}', `ch_flag` = '{self.checkflag}' WHERE `tgid` = '{self.id}'"
                c.execute(sql)
                db.connection.commit()
        except Exception as e:
            print(e)
        
    def delete(self):
        try:
            with db.connection.cursor() as c:
                sql = f"DELETE FROM `users` WHERE `tgid` = '{self.id}'"
                c.execute(sql)
                db.connection.commit()
        except Exception as e:
            print(e)
        
    def __del__(self):
        self.__savedata()

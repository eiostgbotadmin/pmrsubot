#!/usr/bin/env python
# -*- coding: utf-8 -*-
import telebot, db
from botmain import bot
from collections import deque
import pmrsu

u_list = deque()

def fill_queue():
    u_list.clear()
    try:
        with db.connection.cursor() as c:
            sql = "SELECT tgid,login,pass FROM users"
            c.execute(sql)
            list_ = c.fetchall()
            for el in list_:
                u_list.append(el)
            if not u_list:
                return 0
            return 1
    except:
        return 0
    
def go_messages():
    q = fill_queue()
    if q == 0:
        return 0   
    while True:
        try:
            s_data = u_list.popleft()
            student = pmrsu.StudentProfile(s_data['login'],s_data['pass'])
            if(student.get_lstate() == 0):
                continue
            ttable = student.getTimeTableFull()
            response_ttable = ''
            if(not ttable):
                response_ttable = '<b>Пар сегодня нет :)</b>'
            else:
                for i in ttable:
                    response_ttable += '{0} \n\n'.format(i)
            bot.send_message(s_data['tgid'], '<b>Доброе утро, студент!</b> 😎 \n\n<i>Расписание на сегодня:</i>', parse_mode="HTML")
            bot.send_message(s_data['tgid'], response_ttable, parse_mode="HTML")
        except IndexError:
            break

if __name__ == "__main__":
    go_messages()

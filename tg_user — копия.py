#!/usr/bin/env python
# -*- coding: utf-8 -*-

import db
import json #json для сохранения данных состояния

users_dir = "users_cfg/" #директория с файлами данных пользователей

class User(): #пользователь
    
    def __init__(self,uid): #инициализация
        self.id = uid
        self.fname = users_dir + str(self.id) + ".json"
        self.login = ''
        self.pwd = ''
        self.sost = 0
        self.smsg = 0
        self.getdata() #получение данных из файла настроек
        
    def getdata(self): #получение данных из файла настроек
        try:
            in_file = open(self.fname, "r")
            u_data = json.load(in_file)
            self.setsost(u_data['sost'])
            self.setlogin(u_data['login'])
            self.setpass(u_data['password'])
            self.smsg = u_data['smsg']
            in_file.close()
        except FileNotFoundError:
            pass
    
    def setlogin(self, login): 
        self.login = login
        self.savedata()
    def setpass(self, pwd):
        self.pwd = pwd
        self.savedata()
    
    def msg_dset(self,smsg):
        self.smsg = smsg
    def msg_dget(self):
        return self.smsg
        
    def getlogin(self): 
        return self.login
    def getpass(self):
        return self.pwd
        
    def getid(self):
        return self.id
    
    def setsost(self,sost):
        self.sost = sost;
        self.savedata()
    def getsost(self):
        return self.sost
    
    def savedata(self): #сохранение настроек
        self.d = dict(uid=self.id, sost=self.sost, login=self.login, password=self.pwd, smsg=self.smsg)
        with open(self.fname, "w") as wfile:
            json.dump(self.d, wfile)
            wfile.close()
    def delete(self):
        try:
            os.remove(self.fname)
            return 1
        except:
            return 0
        
    def __del__(self):
        self.savedata()
